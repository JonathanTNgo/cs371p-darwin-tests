*** Darwin 1x5 ***
Turn = 0.
  01234
0 hh..h

Turn = 1.
  01234
0 hh..h

Turn = 2.
  01234
0 hh..h

Turn = 3.
  01234
0 hh..h

Turn = 4.
  01234
0 hh..h

*** Darwin 6x4 ***
Turn = 0.
  0123
0 ....
1 hr.t
2 ....
3 ....
4 ff.t
5 ....

Turn = 1.
  0123
0 .r..
1 h..t
2 ....
3 ....
4 ff.t
5 ....

Turn = 2.
  0123
0 .r..
1 h..t
2 ....
3 ....
4 ff.t
5 ....

Turn = 3.
  0123
0 ..r.
1 h..t
2 ....
3 ....
4 ff.t
5 ....

Turn = 4.
  0123
0 ...r
1 h..t
2 ....
3 ....
4 ff.t
5 ....

Turn = 5.
  0123
0 ...t
1 h..t
2 ....
3 ....
4 ff.t
5 ....

Turn = 6.
  0123
0 ...t
1 h..t
2 ....
3 ....
4 ff.t
5 ....

Turn = 7.
  0123
0 ...t
1 h..t
2 ....
3 ....
4 ff.t
5 ....

Turn = 8.
  0123
0 ...t
1 h..t
2 ....
3 ....
4 ff.t
5 ....

Turn = 9.
  0123
0 ...t
1 h..t
2 ....
3 ....
4 ff.t
5 ....

Turn = 10.
  0123
0 ...t
1 h..t
2 ....
3 ....
4 ff.t
5 ....

*** Darwin 1x1 ***
Turn = 0.
  0
0 h

Turn = 4.
  0
0 h

Turn = 8.
  0
0 h

*** Darwin 2x6 ***
Turn = 0.
  012345
0 h.t.rf
1 rt.ht.

Turn = 3.
  012345
0 rrrr.f
1 .th.t.

Turn = 6.
  012345
0 .rr..f
1 rrrrt.

*** Darwin 1x1 ***
Turn = 0.
  0
0 r

Turn = 2.
  0
0 r

*** Darwin 3x3 ***
Turn = 0.
  012
0 ...
1 ...
2 h..

Turn = 7.
  012
0 ...
1 ...
2 ..h

*** Darwin 6x2 ***
Turn = 0.
  01
0 t.
1 .r
2 .t
3 t.
4 .f
5 hf

Turn = 2.
  01
0 t.
1 .r
2 ..
3 tr
4 hf
5 .f

*** Darwin 6x2 ***
Turn = 0.
  01
0 ..
1 ..
2 ff
3 t.
4 f.
5 .f

Turn = 2.
  01
0 ..
1 ..
2 ff
3 t.
4 t.
5 .f

*** Darwin 1x6 ***
Turn = 0.
  012345
0 .t...r

Turn = 3.
  012345
0 .t...r

Turn = 6.
  012345
0 .t..r.

*** Darwin 3x5 ***
Turn = 0.
  01234
0 h....
1 .....
2 .....

Turn = 1.
  01234
0 .....
1 h....
2 .....

*** Darwin 5x5 ***
Turn = 0.
  01234
0 .r...
1 .....
2 .rtft
3 ....f
4 fh...

Turn = 7.
  01234
0 .....
1 .....
2 .tttt
3 ...tt
4 r..r.

*** Darwin 7x4 ***
Turn = 0.
  0123
0 ....
1 ...r
2 f..f
3 ft.f
4 ....
5 ....
6 ...f

Turn = 2.
  0123
0 ...r
1 ....
2 f..f
3 ft.f
4 ....
5 ....
6 ...f

*** Darwin 7x7 ***
Turn = 0.
  0123456
0 .......
1 .......
2 .tf....
3 .......
4 .t.....
5 r.....f
6 .....f.

Turn = 1.
  0123456
0 .......
1 .......
2 .tf....
3 .......
4 .t.....
5 ......f
6 r....f.

*** Darwin 5x7 ***
Turn = 0.
  0123456
0 .......
1 .t.....
2 .......
3 .......
4 h......

Turn = 2.
  0123456
0 .......
1 .t.....
2 .......
3 .......
4 h......

*** Darwin 5x5 ***
Turn = 0.
  01234
0 t.h..
1 h..r.
2 .ht..
3 .....
4 .f...

Turn = 1.
  01234
0 t.h..
1 .h..r
2 h.t..
3 .....
4 .f...

Turn = 2.
  01234
0 t.h..
1 ..h.r
2 h.t..
3 .....
4 .f...

Turn = 3.
  01234
0 t.h..
1 ...h.
2 h.t.r
3 .....
4 .f...

Turn = 4.
  01234
0 t.h..
1 ....h
2 h.t..
3 ....r
4 .f...

Turn = 5.
  01234
0 t.h..
1 ....h
2 h.t..
3 .....
4 .f..r

Turn = 6.
  01234
0 t.h..
1 ....h
2 h.t..
3 .....
4 .f..r

Turn = 7.
  01234
0 t.h..
1 ....h
2 h.t..
3 .....
4 .f..r

*** Darwin 7x1 ***
Turn = 0.
  0
0 r
1 .
2 f
3 h
4 .
5 .
6 .

Turn = 8.
  0
0 r
1 .
2 .
3 .
4 .
5 r
6 h

*** Darwin 1x5 ***
Turn = 0.
  01234
0 .ttrt

Turn = 2.
  01234
0 .tttt

Turn = 4.
  01234
0 .tttt

*** Darwin 2x4 ***
Turn = 0.
  0123
0 rt.h
1 .hh.

Turn = 5.
  0123
0 .t..
1 tttt

*** Darwin 6x7 ***
Turn = 0.
  0123456
0 ....h.r
1 ...h...
2 ..r....
3 rr.....
4 .....f.
5 ......r

Turn = 2.
  0123456
0 .......
1 .r.h...
2 ....h.r
3 .......
4 ..r..f.
5 r...r..

Turn = 4.
  0123456
0 .r.....
1 .....h.
2 .......
3 .......
4 ....hfr
5 r.rr...

Turn = 6.
  0123456
0 r......
1 ......h
2 .......
3 r..r...
4 .....f.
5 r...h.r

Turn = 8.
  0123456
0 r......
1 r..r..h
2 .......
3 .......
4 r....f.
5 ....h.r

*** Darwin 4x6 ***
Turn = 0.
  012345
0 ..t.r.
1 ..h.tr
2 r...t.
3 ..t..r

Turn = 9.
  012345
0 r.....
1 r..ttt
2 ..t.tt
3 ..t...

*** Darwin 6x4 ***
Turn = 0.
  0123
0 ....
1 t...
2 ....
3 ....
4 ...t
5 ....

Turn = 1.
  0123
0 ....
1 t...
2 ....
3 ....
4 ...t
5 ....

Turn = 2.
  0123
0 ....
1 t...
2 ....
3 ....
4 ...t
5 ....

*** Darwin 5x7 ***
Turn = 0.
  0123456
0 .....h.
1 .th....
2 .......
3 r....t.
4 .......

Turn = 1.
  0123456
0 ..h...h
1 .t.....
2 r......
3 .....t.
4 .......

Turn = 2.
  0123456
0 ..h...h
1 rt.....
2 .......
3 .....t.
4 .......

Turn = 3.
  0123456
0 r.h...h
1 .t.....
2 .......
3 .....t.
4 .......

Turn = 4.
  0123456
0 r.h...h
1 .t.....
2 .......
3 .....t.
4 .......

Turn = 5.
  0123456
0 .rh...h
1 .t.....
2 .......
3 .....t.
4 .......
